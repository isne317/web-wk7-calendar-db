<style>

div.calendar
{
	position: relative;
	width: 960px;
}

div.days
{
	width: 120px;
	height: 26px;
	font-size: 22px;
	font-weight: bold;
	float: left;
	text-align: center;
	text-transform: capitalize;
	border: 1px solid black;
	padding: 2px 0;
}

div.dateName
{
	border: 1px solid black;
	float: left;
	width: 112px;
	height: 16px;
	font-size: 14px;
	font-weight: bold;
	text-align: right;
	padding: 2px 4px;
}
.Nnorm
{
	background-color: #dddddd;
}
.Ntoday
{
	background-color: #01FF70;
}

div.dateBlock
{
	border: 1px solid black;
	float: left;
	width: 112px;
	height: 92px;
	padding: 4px 4px;
	word-wrap: normal;
}
.Bnorm
{
	background-color: #FFFFFF;
}
.Btoday
{
	background-color: #01FF70;
}
.Bnull
{
	background-color: #dddddd;
}
input[type=submit]
{
	border:none;
	background-color: transparent;
	cursor: pointer;
}

</style>

<?php

date_default_timezone_set("Asia/Bangkok");
$day = date('d',time());
$month = $_GET['m'];
$year = $_GET['y'];
$noteDate = array();
$noteTitle = array();
$noteDesc = array();
$mysqli = new mysqli("localhost","root","","schedules");
$qry = "SELECT * FROM `schedules` ORDER BY `schedules`.`date` ASC";
$result = $mysqli->query($qry);
while($row = $result->fetch_array())
{
	if(date('m',strtotime($row[1])) == $month && date('Y',strtotime($row[1])) == $year)
	{
		$noteDate[count($noteDate)] = date('d',strtotime($row[1]));
		$noteTitle[count($noteTitle)] = $row[2];
		$noteDesc[count($noteDesc)] = $row[3];
	}
}
$noteDate[count($noteDate)] = -2;
$mysqli->close();

echo "<h2>";
echo "<a href='calendar.php?m=". $month ."&y=" . ($year-1) . "'> << &nbsp</a>";
echo "<a href='calendar.php?m=". ($month-1==0?12:$month-1) ."&y=" . ($month-1==0?$year-1:$year) . "'> < &nbsp</a>";
echo "Calendar of " . date('F, Y', strtotime('01-' . $month . '-' . $year)) . " &nbsp";
echo "<a href='calendar.php?m=". ($month+1==13?01:$month+1) ."&y=" . ($month+1==13?$year+1:$year) . "'> > &nbsp</a>";
echo "<a href='calendar.php?m=". $month ."&y=" . ($year+1) . "'> >> &nbsp</a>";
echo "</h2>";

?>

<head>
<title><?php echo "Calendar of " . date('F, Y', strtotime('01-' . $month . '-' . $year)); ?></title>
</head>

<div class="calendar">

<?php

$ind = 0;
$daysArray = array();
$firstDay = date('w',strtotime('01-' . $month . '-' . $year));
$days = array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
foreach($days as $i)
{
	echo "<div class='days'>" . $i . "</div>";
}
for($i = 0; $i < $firstDay; $i++)
{
	$daysArray[$i] = ' ';
}
for($i = 1; date('m',strtotime($i . '-' . $month . '-' . $year)) == $month && $i < 32; $i++)
{
	$daysArray[$firstDay + ($i-1)] = $i;
}
while(count($daysArray) % 7 != 0)
{
	$daysArray[count($daysArray)] = ' ';
}
for($i = 0; $i < count($daysArray); $i++)
{
	if($daysArray[$i] == ' ')
	{
		echo "<div class='dateName Nnorm'> </div>";
	}
	elseif($daysArray[$i] == $day && $month == date('m',time()) && $year == date('Y',time()))
	{
		echo "<div class='dateName Ntoday'>Today - " . $day . "</div>";
	}
	else
	{
		echo "<div class='dateName Nnorm'>" . $daysArray[$i] . "</div>";
	}
	if(($i+1) %7 == 0 && $i != 0)
	{
		for($j = $i-6; $j <= $i; $j++)
		{
			if($daysArray[$j] == ' ')
			{
				echo "<div class='dateBlock Bnull'> ";
			}
			elseif($daysArray[$j] == $day && $month == date('m',time()) && $year == date('Y',time()))
			{
				echo "<div class='dateBlock Btoday'>";
			}
			else
			{
				echo "<div class='dateBlock Bnorm'>";
			}
			if($noteDate[$ind] == $daysArray[$j])
			{
				echo "<form action='info.php' method='POST'>";
				echo "<input type='submit' value='".$noteTitle[$ind]."'>" ;
				echo "<input name='title' type='hidden' value='".$noteTitle[$ind]."'>" ;
				echo "<input name='desc' type='hidden' value='".$noteDesc[$ind]."'>";
				echo "<input name='date' type='hidden' value='".$noteDate[$ind]."-".$month."-".$year."'>";
				echo "</form>";
				$ind++;
			}
			echo "</div>";
		}
	}
}

?>
</div>

&nbsp<br>&nbsp<br>

<p id="test"> </p>

