<style>

input[type=text]
{
	width: 100%;
	padding: 4px 4px;
	border-radius: 2px;
	font-size: 1em;
	margin: 4px 0;
}

input[type=date]
{
	width: 100%;
	padding: 4px 4px;
	border-radius: 2px;
	font-size: 1em;
	margin: 4px 0;
}

input[type=submit]
{
	width: 100%;
	padding: 8px 4px;
	border: none;
	border-radius: 2px;
	font-size: 1em;
	margin: 4px 0;
	color: white;
	background-color: #2ECC40;
}

input[type=reset]
{
	width: 100%;
	padding: 8px 4px;
	border: none;
	border-radius: 2px;
	font-size: 1em;
	margin: 4px 0;
	color: white;
	background-color: #FF4136;
}

</style>

<form action="redirect.php" method="POST">
<input type="date" name="date">
<input type="text" name="title" placeholder="Title (Max 40 characters)">
<input type="text" name="desc" placeholder="Description">
<input type="submit" value="Send">
<input type="reset" value="Reset">
</form>